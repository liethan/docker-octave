## SPDX-License-Identifier: BSD-3-Clause
## Copyright (C) 2019 Mike Miller
##
## docker-functions.sh - shell functions to abstract building Docker images
##
## These functions are not meant to be generic, they are intentionally specific
## to the Octave image being built by this project.
##
## The following environment or shell variables are required and are assumed
## to be set appropriately before calling these functions.
##
##   * DOCKER_IMAGE    - the name of the Docker image
##   * OCTAVE_VERSIONS - the list of Octave versions, and tags of the image
##   * DOCKER_USERNAME - Docker Hub login username
##   * DOCKER_PASSWORD - Docker Hub login password

## The default version of Octave, will be tagged 'latest'
OCTAVE_LATEST_VERSION=5.1.0

## The development version of Octave, will be tagged 'devel'
OCTAVE_DEVELOPMENT_VERSION=6.0.0

## The latest versions of each stable series of Octave included in this image
OCTAVE_LATEST_VERSIONS="4.2.2:4.4.1:5.1.0"

## Return the abbreviated form of OCTAVE_VERSION. This is used to name one or
## more tags that track the latest release of an Octave stable release branch,
## if and only if OCTAVE_VERSION is listed in OCTAVE_LATEST_VERSIONS above.
get_versions_abbreviated ()
{
  case "$OCTAVE_VERSION" in
    [234].*) echo "$OCTAVE_VERSION" | sed 's|^\([234]\.[0-9]\+\)\..*|\1|';;
    *.0.*)   echo "$OCTAVE_VERSION" | sed 's|^\([0-9]\+\)\..*|\1|';;
    *)       echo "$OCTAVE_VERSION" | sed 's|^\([0-9]\+\)\.\([0-9]\+\)\..*|\1.\2 \1|';;
  esac
}

docker_build_image ()
{
  for OCTAVE_VERSION in $OCTAVE_VERSIONS; do
    docker build --build-arg=OCTAVE_VERSION=$OCTAVE_VERSION -t ${DOCKER_IMAGE}:${OCTAVE_VERSION} .
  done
}

docker_export_image_artifact ()
{
  mkdir -p images
  images=
  for OCTAVE_VERSION in $OCTAVE_VERSIONS; do
    images="$images ${DOCKER_IMAGE}:${OCTAVE_VERSION}"
  done
  docker save $images > images/octave.tar
}

docker_import_image_artifact ()
{
  docker load < images/octave.tar
}

docker_run_command ()
{
  docker run ${DOCKER_IMAGE}:${OCTAVE_VERSION} "$@"
}

docker_test_image ()
{
  for OCTAVE_VERSION in $OCTAVE_VERSIONS; do
    (
      set -xe
      test x"$(docker_run_command octave --eval "disp (OCTAVE_VERSION)")" = x"$OCTAVE_VERSION"
      docker_run_command octave --version
      docker_run_command octave --eval "ver"
      docker_run_command octave --eval "test ('assert')"
      docker_run_command git --version
      docker_run_command python2 --version
      docker_run_command python3 --version
      docker_run_command pip2 --version
      docker_run_command pip3 --version
    )
  done
}

docker_push_image_and_tags ()
{
  echo "$DOCKER_PASSWORD" | docker login -u "$DOCKER_USERNAME" --password-stdin

  for OCTAVE_VERSION in $OCTAVE_VERSIONS; do
    docker push ${DOCKER_IMAGE}:${OCTAVE_VERSION}

    ## If OCTAVE_VERSION is the latest release in its series, then push an alias
    if echo ":${OCTAVE_LATEST_VERSIONS}:" | grep -F ":${OCTAVE_VERSION}:" > /dev/null; then
      for SHORT_VERSION in $(get_versions_abbreviated); do
        test x"$SHORT_VERSION" != x && test x"$SHORT_VERSION" != x"$OCTAVE_VERSION"
        docker tag ${DOCKER_IMAGE}:${OCTAVE_VERSION} ${DOCKER_IMAGE}:${SHORT_VERSION}
        docker push ${DOCKER_IMAGE}:${SHORT_VERSION}
      done
    fi

    if [ x"$OCTAVE_VERSION" = x"${OCTAVE_LATEST_VERSION}" ]; then
      docker tag ${DOCKER_IMAGE}:${OCTAVE_VERSION} ${DOCKER_IMAGE}:latest
      docker push ${DOCKER_IMAGE}:latest
    fi

    if [ x"$OCTAVE_VERSION" = x"$OCTAVE_DEVELOPMENT_VERSION" ]; then
      docker tag ${DOCKER_IMAGE}:${OCTAVE_VERSION} ${DOCKER_IMAGE}:devel
      docker push ${DOCKER_IMAGE}:devel
    fi
  done
}
